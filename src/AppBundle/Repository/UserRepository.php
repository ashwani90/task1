<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 28/6/18
 * Time: 2:39 PM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function updateNotify($user_id)
    {
        $qb = $this->createQueryBuilder('p')
            ->update(User::class, 'u')
            ->set('u.notify','1')
            ->Where('u.id = :user_id')
            ->setParameter('user_id',$user_id)
            ->getQuery();
        return $qb->execute();
    }


}