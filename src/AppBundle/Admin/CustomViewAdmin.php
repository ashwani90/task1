<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/6/18
 * Time: 2:36 PM
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class CustomViewAdmin
 * @package AppBundle\Admin
 */
class CustomViewAdmin extends AbstractAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'custom_view';
    /**
     * @var string
     */
    protected $baseRouteName = 'custom_view';

    /**
     * @param RouteCollection $collection
     * removing all the routes except the list route
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}