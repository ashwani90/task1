<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/6/18
 * Time: 6:32 PM
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;

/**
 * Class RelativeAdmin
 * @package AppBundle\Admin
 */
class RelativeAdmin extends Admin
{
    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'AppBundle/admin/list.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }
}