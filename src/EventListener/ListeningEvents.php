<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 15/6/18
 * Time: 10:25 AM
 */

class ListeningEvents
{
    private $logger;
    public function __construct(\Symfony\Component\HttpKernel\Log\DebugLoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    public function onKernelRequest($event)
    {
        $this->logger->info("This function was called");
    }


}