<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/6/18
 * Time: 1:20 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Form\RegistrationFormType as BaseReg;

/**
 * Class RegistrationFormType
 * @package AppBundle\Form
 */
class RegistrationFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * to add a new parameter i.e. firstname and removing the username
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName')->remove('username');;
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }


}