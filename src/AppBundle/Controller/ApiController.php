<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 12/6/18
 * Time: 4:35 PM
 */

namespace AppBundle\Controller;

use Psr\Log\LoggerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Operator;
use AppBundle\Entity\Voucher;

/**
 * Class ApiController
 * @package AppBundle\Controller
 * This class is being used to create rest apis
 */
class ApiController extends FOSRestController
{
    /**
     * @Rest\Post("api/operator")
     * creates a new operator
     * @param Request
     * @return View
     */
    public function postAction(Request $request)
    {

        $operator_name = $request->request->get('operator_name');
        $net_id = $request->request->get('net_id');

        if(empty($net_id) || empty($operator_name))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $net_data = $this->getDoctrine()->getRepository('AppBundle:Operator')->findElWithNetID($net_id);

        if($net_data != null)
        {
            return new View("Network Id already Exists", Response::HTTP_NOT_ACCEPTABLE);
        }
            $ins_data = $this->getDoctrine()->getRepository("AppBundle:Operator")
                ->createOperator($operator_name,$net_id);
        if($ins_data)
        {
            return new View("Operator Added Successfully", Response::HTTP_OK);
        }
        else
        {
            return new View("Some internal server error occured", 500);
        }
    }

    /**
     * @Rest\Get("api/operator")
     * gets all operators
     *
     */
    public function getOperatorAction()
    {
        $operators = $this->getDoctrine()->getRepository('AppBundle:Operator')->findAll();
        if ($operators === null) {
            return new View("There are no operators", Response::HTTP_NOT_FOUND);
        }
        return $operators;
    }

    /**
     * @Rest\Get("api/operator/{id}")
     * gets one operator
     */
    public function getSingleOperatorAction($id)
    {
        $operator = $this->getDoctrine()->getRepository('AppBundle:Operator')->find($id);
        if ($operator === null) {
            return new View("There is no operator with this id", Response::HTTP_NOT_FOUND);
        }
        return $operator;
    }

    /**
     * @Rest\Get("api/voucher/{json_data}")
     * gets vouchers as per the networkId, amount, Quantity
     * @param string
     * @return View
     */
    public function getVouchers($json_data)
    {
        //var_dump($json_data);die;
        $json_data = json_decode($json_data,true);
        //var_dump($json_data);die;
        if(isset($json_data["quantity"]))
        {
            $quantity = $json_data["quantity"];
        }else{
            $quantity = 1;
        }
        $net_id = $json_data["net_id"];
        $amount = $json_data["amount"];
        $voucher = $this->getDoctrine()->getRepository('AppBundle:Voucher')->getVouchers($quantity,$net_id,$amount);
        if ($voucher === null) {
            return new View("There is no operator with this id", Response::HTTP_NOT_FOUND);
        }
        return $voucher;
    }



}