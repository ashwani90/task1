<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 15/6/18
 * Time: 10:36 AM
 */

namespace AppBundle\EventListener;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class ListeningEvents
{
    private $api_token;

    /**
     * ListeningEvents constructor.
     * @param $api_token
     * @param Logger $logger
     */
    public function __construct($api_token , Logger $logger)
    {
        $this->api_token = $api_token;
        $this->logger = $logger;
    }

    /**
     * @param GetResponseEvent $event
     * used to log the request to logger and authentication
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $this->logger->info("Request ".$event->getRequest()->getContent());
        $api_key = $event->getRequest()->headers->get('Authorization');

//        if($api_key != $this->api_token)
//        {
//            $this->logger->info("Invalid Request");
//            $response = new Response();
//            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
//            $response->setContent("Invalid Credentials");
//            $event->setResponse($response);
//        }else{
//
//        }
    }

}