<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/6/18
 * Time: 11:11 AM
 */

namespace AppBundle\EventListener;

use Monolog\Logger;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class ResponseListener
 * @package AppBundle\EventListener
 * listens to the kernel.response event
 */
class ResponseListener
{

    /**
     * @var Logger
     * to log to the reponse file
     */
    private $logger;

    /**
     * ResponseListener constructor.
     * @param Logger $logger
     * to inject the response channel
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }
    /**
     * @param FilterResponseEvent $event
     * logs the reponse to the responses log file using response log channel
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
//        $response = new Response();
//        $response->setContent(json_encode($r));
//        $event->setResponse($response);
        $this->logger->info("Request ".$event->getRequest()->getContent());
        $this->logger->info("Response ".$event->getResponse()->getContent());
    }

}