<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 15/6/18
 * Time: 12:08 PM
 */

namespace AppBundle\Admin;

use AppBundle\Entity\Operator;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class OperatorAdmin
 * @package AppBundle\Admin
 */
class OperatorAdmin extends AbstractAdmin
{
    /**
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     * disabling distinct from the sonata listing query
     */
    public function createQuery($context='list')
    {
        $query = parent::createQuery($context);
        $query->setDistinct(false);
        return $query;
    }
    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form->add('name', TextType::class);
        $form->add('networkId',TextType::class);
    }

    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('name');
        $filter->add('networkId');
    }

    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('name');
        $list->addIdentifier('networkId');
    }

    /**
     * @param $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Operator
            ? $object->getName()
            : 'Operator'; // shown in the breadcrumb on the create view
    }

}