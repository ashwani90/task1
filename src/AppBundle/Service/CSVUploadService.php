<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 13/6/18
 * Time: 12:38 PM
 */

namespace AppBundle\Service;


use AppBundle\Entity\Voucher;
use Doctrine\ORM\EntityManager;

/**
 * Class CSVUploadService
 * @package AppBundle\Service
 * this class is a service to upload csv data
 */
class CSVUploadService
{
    /**
     * @var EntityManager
     * object to interact with database
     */
    private $entityManager;

    /**
     * CSVUploadService constructor.
     * @param EntityManager $entityManager
     * instantiation of the object
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $csv
     * @param $networkId
     * @param $expirydate
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     * uploads the array of vouchers into database
     */
    public function uploadCSV($csv, $networkId, $expirydate)
    {

        for($i=0;$i<count($csv);$i++)
        {

            $op = $this->entityManager->getRepository("AppBundle:Operator")
                ->findElWithNetID($networkId);
            if($op == null)
            {
                return array('status'=>"Invalid Network Id");
            }
            $op_id = $op[0];
            $serial_no = $csv[$i][0];
            $set_ser = $this->entityManager->getRepository("AppBundle:Voucher")
                ->findWithSerialNo($serial_no);
            if($set_ser != null)
            {
                continue;
            }
            $amount = 100;
            $operator_id = $op_id;
            $state = 1;
            $voucher = new Voucher();
            $voucher->setAmount($amount);
            $voucher->setSerialnumber($serial_no);
            $voucher->setExpirydate(new \DateTime($expirydate));
            $voucher->setState($state);
            $voucher->setOperator($operator_id);
            $this->entityManager->persist($voucher);
            $this->entityManager->flush();
        }
        return array('status'=>"Data saved Successfully");
    }


}