<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/subscribe/{user_id}", name="subscribe")
     * @param Request $request
     * @param $user_id
     */
    public function subscribeAction(Request $request,$user_id)
    {

        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($user_id);
        $user->setNotify(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        //dump($request->headers->get('referer'));die;
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {

    }
}
