========================
Software Requirements:
    * Ubuntu Server 16.04 
    * Apache 2.4.*
    * MySQL 5.7.*
    * PHP 7.*
    

What's inside?
--------------
    * Rest apis to create operator, get operator and create vouchers

Installation: 
    * git clone https://ashwani90@bitbucket.org/ashwani90/task1.git 
    * composer install
    * php bin/console assets:install
    * php bin/console cache:clear
    * php bin/console doctrine:database:create
    * php bin/console doctrine:migrations:diff
    * php bin/console doctrine:migrations:migrate
     
The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

Bundles Used:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev env) - Adds code generation
    capabilities

  * [**WebServerBundle**][14] (in dev env) - Adds commands for running applications
    using the PHP built-in web server

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration
    
  * **FOSRestBundle**  - To create rest apis
  
  * **SerializerBundle**  - for serializing objects
  
  * **NelmioCorsBundle** - allows to send cross origin requests
  
  * **SonataBundle** - to create admin of the application
  
  * **FOSUserBundle** - for user management
