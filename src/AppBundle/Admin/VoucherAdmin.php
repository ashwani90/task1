<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 15/6/18
 * Time: 12:34 PM
 */

namespace AppBundle\Admin;


use AppBundle\Entity\Operator;
use AppBundle\Entity\Voucher;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;


/**
 * Class VoucherAdmin
 * @package AppBundle\Admin
 */
class VoucherAdmin extends AbstractAdmin
{

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {

        $form
            ->with('Content', ['class' => 'col-md-9'])
            ->add('serialnumber', TextType::class)
            ->add('amount', IntegerType::class)
            ->add('expirydate', DateTimeType::class)
            ->end()

            ->with('Meta data', ['class' => 'col-md-3'])
            ->add('operator', ModelType::class, [
                'class' => Operator::class,
                'property' => 'name',
            ])
            ->add('state',BooleanType::class,array('choices'=>[
           "Enabled"=>"1",
           "Disabled"=>"0"
       ]))
            ->end()
        ;

    }

    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('serialnumber')->add('operator', null, [], EntityType::class, [
            'class'    => Operator::class,
            'choice_label' => 'name',
        ]);
    }

    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('serialnumber')->add('amount')->add('expirydate')
            ->add('state')->add('operator.name');
    }

    /**
     * @param $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Voucher
            ? $object->getSerialnumber()
            : 'Voucher'; // shown in the breadcrumb on the create view
    }
}