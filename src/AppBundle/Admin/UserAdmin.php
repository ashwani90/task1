<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 27/6/18
 * Time: 4:53 PM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use AppBundle\Entity\User;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

class UserAdmin extends AbstractAdmin
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * configuring search filters for users
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('email','',array('label'=>"Email"));
    }

    /**
     * configuring list fields for user
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list)
    {
        $list->add('id','',array('label'=>'ID'))
            ->add('email','',array('label'=>'Email'))
            ->add('username','',array('label'=>'Username'))
            ->add('enabled')
            ->add('last_login','datetime',array('label'=>'Last Login'))
        ->add('_action', null, [
            'actions'=> [
                'show'=>[],
                'edit'=>[],
                'delete'=>[]
            ]
        ]);
    }

    /**
     * configuring form fields for users
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form->add('email','text',array('label'=>'Email'))
            ->add('username','text',array('label'=>'Username'))
            ->add('enabled',null,array('label'=>'Status'))
            ->add('password','password',array('label'=>'Password'))
            ->add('roles','choice',
                [
                    'choices'=>[
                        'Super Admin'=>'ROLE_SUPER_ADMIN',
                        'Admin'=>'ROLE_ADMIN',
                        'User'=>'ROLE_USER'
                    ],
                    'expanded' => true,
                    'multiple' => true,
                ]);

    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->add('id','', array('label'=>'Id'))
            ->add('enabled','', array('label'=>'enabled'))
            ->add('username','', array('label'=>'Username'))
            ->add('email','', array('label'=>'Email'))
        ->add('operator.name','', array('label'=>'Operator Name'));
    }

    /**
     * generating a random password
     * @return bool|string
     */
    public function generateRandomPassword()
    {
        return substr(str_shuffle("abcdefghijklmnopqrstuvwxyzBCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
    }

    /**
     * @param User $object
     * @throws \Exception
     */
    public function prePersist($object)
    {

        $form = $this->getForm();
        $object->setUsernameCanonical($form->get('username')->getData());
        $object->setEmailCanonical($form->get('email')->getData());
        $object->setEmail($form->get('email')->getData());
        $object->setPassword($this->encodePassword($object, $form->get('password')->getData()));
        $object->setFirstName("so");
        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        //starting the transaction
        $em->getConnection()->beginTransaction();
        $em->persist($object);
        try {

            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            $em->close();
            throw $e;
        }
    }

    /**
     * @param User $object
     * @throws \Exception
     */
    public function preUpdate($object)
    {

        $form = $this->getForm();
        $object->setUsernameCanonical($form->get('username')->getData());
        $object->setEmailCanonical($form->get('email')->getData());
        $object->setEmail($form->get('email')->getData());
        $object->setPassword($this->encodePassword($object, $form->get('password')->getData()));
        $object->setFirstName("so");
        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        //starting the transaction
        $em->getConnection()->beginTransaction();
        $em->persist($object);
        try {
            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            $em->close();
            throw $e;
        }
    }

//    /**
//     * @param $user
//     */
//    protected function setUserFields($user)
//    {
//        $userManager = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
//        $userManager->updateCanonicalFields($user);
//
//        $userManager->updatePassword($user);
//    }


    /**
     * @param User $user
     * @param $password
     * @return mixed
     */
    private function encodePassword(User $user, $password)
    {
        $this->container = $this->getConfigurationPool()->getContainer();
        $encoder = $this->container->get('security.password_encoder');
        return $encoder->encodePassword($user,$password);
    }

    public function getExportFields()
    {
        return [  'email' , 'id' ];

    }

}