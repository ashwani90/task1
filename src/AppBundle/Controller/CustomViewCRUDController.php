<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/6/18
 * Time: 2:38 PM
 */

namespace AppBundle\Controller;


use Sonata\AdminBundle\Controller\CRUDController;

/**
 * Class CustomViewCRUDController
 * @package AppBundle\Controller
 * using sonata crud controller
 */
class CustomViewCRUDController extends CRUDController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->renderWithExtraParams('admin/custom_view.html.twig');
    }
}